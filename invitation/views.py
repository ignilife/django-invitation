# -*- coding: utf-8 -*-

from django.conf import settings
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect

from invitation.models import InvitationKey

is_key_valid = InvitationKey.objects.is_key_valid


def invited(request, invitation_key=None, extra_context=None):
    if getattr(settings, 'INVITE_MODE', False):
        if invitation_key and is_key_valid(invitation_key):
            template_name = 'invitation/invited.html'
        else:
            template_name = 'invitation/wrong_invitation_key.html'
        if extra_context is None:
            extra_context = {}
        else:
            extra_context = extra_context.copy()
        extra_context.update({'invitation_key': invitation_key})
        return render(request, template_name, extra_context)
    else:
        return HttpResponseRedirect(reverse('registration_register'))


# def register(request, backend, success_url=None,
#              form_class=RegistrationForm,
#              disallowed_url='registration_disallowed',
#              post_registration_redirect=None,
#              template_name='registration/registration_form.html',
#              wrong_template_name='invitation/wrong_invitation_key.html',
#              extra_context=None):
#     extra_context = extra_context is not None and extra_context.copy() or {}
#     if getattr(settings, 'INVITE_MODE', False):
#         invitation_key = request.REQUEST.get('invitation_key', False)
#         if invitation_key:
#             extra_context.update({'invitation_key': invitation_key})
#             if is_key_valid(invitation_key):
#                 return registration_register(request, backend, success_url,
#                                              form_class, disallowed_url,
#                                              template_name, extra_context)
#             else:
#                 extra_context.update({'invalid_key': True})
#         else:
#             extra_context.update({'no_key': True})
#         return render(request, wrong_template_name, extra_context)
#     else:
#         return registration_register(
#             request, backend, success_url, form_class, disallowed_url,
#             template_name, extra_context)
